Mari kita coba berkolaborasi dengan Git! Malam ini kita akan berkolaborasi pada satu file html.

Langkah-langkah berkolaborasi dengan Git:

1. Buat sebuah folder di Documents kalian bernama "office-hour" lalu pindah ke direktori folder tersebut dan lakukan git init
2. Clone repository ini di folder itu melalui SSH atau HTTPS dengan menggunakan git clone.
3. Buat branch baru dengan format "feature/namakalian". Contoh: "feature/yogie"
4. Buka index.html di code editor kalian lalu pada file tersebut, kalian tuliskan nama, kota asal, lalu tuliskan hal yang kalian tidak suka apapun itu dan tambahkan note bila perlu! Bonus points if you guys are using English!
5. Setelah selesai cek status apakah sudah terekam di git.
6. Commit kerjaan kalian dengan format "namakalian - kerjaankalian". Contoh: "yogie - menambakan komplain saya".
7. Sebelum melakukan push, lakukan pull ke branch development terlebih dahulu untuk menghindari konflik
8. Setelah melakukan pull, push kerjaan kalian ke branch kalian
9. Lakukan merge request ke development.
10. Good luck!

Update:
Hebat! Kalian sudah bisa menggunakan Git! Sekarang kita coba berkolaborasi lagi ya! Kali ini kita akan membuat profile page yang diakses dari index yang kita kerjakan kemarin.

Petunjuk:
1. Kalian buat folder baru di folder page dan di folder itu buatlah folder baru dengan nama kalian yang isinya file html dengan format "namakalian.html".
2. Isi page profile kalian dengan nama, ttl, gambar (tidak harus foto kalian), dan 2 truths and 1 lie (2 fakta benar tentang diri kalian dan 1 fakta yang bohong tentang diri kalian). Bonus poin apabila kalian menggunakan basaha inggris dan styling atau css!
3. Hubungkan page profile kalian ke index agar bisa diakses.
4. Break a leg, guys!
